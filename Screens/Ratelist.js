import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


import { HTTP, URLS } from '../Controllers/Network';
import { SearchBar } from 'react-native-elements';

export default class Appointments extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            rates: [],
            filtered: [],
            isFetching: true
        }
    }

    componentDidMount() {
        this.getRateList();
    }


    getRateList() {
        this.setState({
            isFetching: true
        })
        HTTP
            .get(URLS.RATELIST.TESTS)
            .then((res) => {
                let data = res.data
                this.setState({
                    rates: data,
                    filtered: data,
                    isFetching: false
                })
            })
            .catch((err) => {
                console.log('== ERROR');
                console.log(err);
                this.setState({
                    isFetching: false
                })
            });
    }




    filterList() {

        if (!this.state.search || this.state.search.length <= 1) {
            this.setState({
                filtered: this.state.rates
            })
            return
        }
        var filtered = [];
        this.state.rates.forEach(item => {
            if (item.name.toLowerCase().includes(this.state.search.toLowerCase())) {
                filtered.push(item)
            }
        });
        this.setState({
            filtered: filtered
        })
    }

    updateSearch = (search) => {
        this.setState({ search });
        this.filterList();
    };

    onClear = () => {
        this.clearFilters()
    }
    clearFilters() {
        this.setState({
            search: '',
            rates: Rates
        })
    }

    render() {

        const { search } = this.state;

        const renderItem = ({ item }) => (
            <View style={s.item}>
                <View style={s.titleCont}>
                    <Text style={s.itemTitle}>{item.name}</Text>
                    <Text style={s.price}>Rs.{Number(item.price).format()}</Text>
                </View>
                <Text style={s.sampleName}>{item.sample}</Text>
                <View style={s.hCont}>
                    <Ionicons size={20} name='ios-time-outline' color='orange' />
                    <Text style={s.hContVal}>{item.time}</Text>
                </View>
            </View >
        );

        return (
            <View style={s.container}>

                <SearchBar
                    placeholder="Enter a keyword to search..."
                    onChangeText={this.updateSearch}
                    value={search}
                    onClear={this.onClear}
                    lightTheme={true}
                    containerStyle={{
                        padding: 10,
                        backgroundColor: 'white'
                    }}
                    inputContainerStyle={{
                        backgroundColor: '#EEEEEE',
                        height: 40,
                        borderRadius: 40
                    }}
                    inputStyle={{
                        fontSize: 14,
                    }}
                />


                {
                    this.state.filtered.length <= 0 ?
                        <View style={s.noData}>
                            <Image style={s.noDataImg} source={require('../assets/images/not_found.png')} />
                            <Text style={s.NoDataText}>Ratelist not available right now ...</Text>
                        </View>
                        :
                        <FlatList
                            data={this.state.filtered}
                            renderItem={renderItem}
                            keyExtractor={(item, key) => String(key)}
                            onRefresh={() => this.getRateList()}
                            refreshing={this.state.isFetching}
                        />

                }


                <StatusBar style="auto" />
            </View>
        );
    }
}

const s = StyleSheet.create({
    container: {
        flex: 1,
    },
    titleContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 14,
        paddingBottom: 10,
        paddingLeft: 14,
        paddingRight: 14
    },
    pageTitle: {
        fontSize: 30,
        fontWeight: '800'
    },
    filterBtWrapper: {
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 2
    },
    filterBt: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowRadius: 10,
        shadowOpacity: 0.4
    },
    item: {
        padding: 20,
        backgroundColor: 'white',
        borderRadius: 4,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    titleCont: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    price: {
        fontSize: 14,
        fontWeight: '600'
    },
    itemTitle: {
        fontSize: 14,
        fontWeight: '600',
        maxWidth: '70%'
    },
    sampleName: {
        fontSize: 12,
        color: 'gray',
        marginTop: 10
    },
    hCont: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center'
    },
    hContVal: {
        fontSize: 12,
        color: 'black',
        textTransform: 'capitalize',
        marginLeft: 4
    },
    filtersWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 3,
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: '100%',
        height: '100%',
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    filters: {
        backgroundColor: 'white',
        width: '90%',
        padding: 20,
        paddingBottom: 10,
        shadowColor: 'gray',
        shadowRadius: 10,
        shadowOpacity: 0.2,
        borderRadius: 10
    },
    filtersTop: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    filtersTitle: {
        color: 'black',
        fontSize: 18,
        fontWeight: '700',
        marginLeft: 10
    },
    filtersBottom: {
        marginTop: 20,
        borderTopColor: 'lightgray',
        borderTopWidth: 0.5,
        paddingTop: 20
    },
    spacer: {
        height: 20
    },
    filtersActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20
    },
    btWrapper: {
        flex: 1,
        height: 50,
    },
    hSpacer: {
        width: 10,
        height: 50
    },
    noData: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fefefe'
    },
    noDataImg: {
        height: 140,
        resizeMode: 'contain',
    },
    NoDataText: {
        color: 'gray',
        fontSize: 14,
        marginTop: 20
    }
});
