import * as React from 'react';;
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Warning: ...']);


import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Landing from "./Screens/Landing";
import Home from "./Screens/Home";
import Login from "./Screens/Login";
import Register from "./Screens/Register";
import ForgotPasword from "./Screens/ForgotPassword";
import BookAppointment from "./Screens/BookAppointment";
import Scanner from "./Screens/Scanner";
import AppointmentDetail from "./Screens/AppointmentDetail";
import PackageDetail from "./Screens/PackageDetail";
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";

Number.prototype.format = function () {
  let d = 2
  let x = parseFloat(this.valueOf());
  if (!d) return x.toFixed(d); // don't go wrong if no decimal
  return x.toFixed(d).replace(/\.?0+$/, "");
};

const Stack = createStackNavigator();

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'orange',
  },
};


function App() {
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator initialRouteName="Landing">
        <Stack.Screen
          options={{
            title: '', headerShown: false, headerTitleStyle: { color: 'white' },
            headerStyle: { backgroundColor: 'orange' },
            headerTintColor: 'red'
          }}
          name="Home"
          component={Home}
        />
        <Stack.Screen
          options={{ title: '', headerShown: false }}
          name="Landing"
          component={Landing}
        />
        <Stack.Screen
          options={{ title: '', headerShown: false }}
          name="Login"
          component={Login}
        />
        <Stack.Screen
          options={{ title: '', headerShown: false }}
          name="Register"
          component={Register}
        />
        <Stack.Screen
          options={{ title: '', headerShown: false }}
          name="ForgotPassword"
          component={ForgotPasword}
        />
        <Stack.Screen
          options={{ title: 'Book Appointment', headerShown: true }}
          name="Book"
          component={BookAppointment}
        />
        <Stack.Screen
          options={{ title: 'Scanner', headerShown: true }}
          name="Scanner"
          component={Scanner}
        />
        <Stack.Screen
          options={{ title: 'Appointment Details', headerShown: true }}
          name="AppointmentDetail"
          component={AppointmentDetail}
        />
        <Stack.Screen
          options={{ title: 'Package Details', headerShown: true }}
          name="PackageDetail"
          component={PackageDetail}
        />
      </Stack.Navigator>
      <FlashMessage position="bottom" />
    </NavigationContainer>

  );
}

export default App;