import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image, Linking } from 'react-native';
import { GET_FROM_STORE, STORE_KEYS } from '../Controllers/Store';

export default function App({ navigation }) {

    useEffect(() => {
        (async () => {
            GET_FROM_STORE(STORE_KEYS.TOKEN)
                .then(token => {
                    console.log('TOKEN');
                    console.log(token);
                    if (!token) {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        })
                    } else {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Home' }],
                        })
                    }
                })
                .catch(e => {
                    console.log('TOKEN ERROR');
                    console.log(e);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    })
                })
        })();
    }, []);

    return (
        <View style={styles.container}>
            <Image style={styles.logo} source={require('../assets/images/logo2.png')} />
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
    }
})