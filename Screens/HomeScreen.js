import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, SafeAreaView, Dimensions, Linking } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { SliderBox } from "react-native-image-slider-box";
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { SAVE_TO_STORE, STORE_KEYS } from '../Controllers/Store';
import { HTTP, URLS } from '../Controllers/Network';

export default class App extends React.Component {

    constructor(props) {
        super(props);
        var aspectHeight = Dimensions.get('window').width / (16 / 9); // find 16/9 ratio height
        this.state = {
            images: [],
            aspectHeight: aspectHeight,
            navigation: props.navigation,
            banners: []
        };
    }

    componentDidMount() {
        this.getBanners();
    }

    getBanners() {
        HTTP
            .get(URLS.BANNER.LIST)
            .then((res) => {
                var images = [];
                res.data.forEach(item => {
                    images.push(item.img_url)
                });
                this.setState({
                    images: images,
                    banners: res.data
                })

            })
            .catch((err) => {
                console.log('== ERROR');
                console.log(err);
            });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView>
                    <View style={s.container}>

                        <View style={s.titleContainer}>
                            <Image style={s.logo} source={require('../assets/images/nxgen_logo_h.png')} />
                        </View>

                        <View style={s.imagesContainer}>
                            <SliderBox
                                images={this.state.images}
                                sliderBoxHeight={this.state.aspectHeight}
                                dotColor="#FFA726"
                                imageLoadingColor="#FFA726"
                                autoplay
                                circleLoop
                                onCurrentImagePressed={index => {
                                    let banner = this.state.banners[index];
                                    console.log(banner);
                                    if (banner.redirect_link && banner.redirect_link != '') {
                                        Linking.openURL(banner.redirect_link)
                                    }
                                }}
                                dotStyle={{
                                    width: 6,
                                    height: 6,
                                }}
                            />
                        </View>

                        <Text style={s.SubTitle}>Services</Text>

                        <View style={s.servicesContainer}>
                            <Grid>
                                <Col >
                                    <Row style={s.row}>
                                        <TouchableOpacity style={s.rowInner} onPress={() => {
                                            this.state.navigation.navigate('Appointments');
                                        }}>
                                            <Ionicons name='ios-file-tray-stacked-outline' size={32} color='orange' />
                                            <Text style={s.title}>My Reports</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={s.row}>
                                        <TouchableOpacity style={s.rowInner} onPress={() => {
                                            SAVE_TO_STORE(STORE_KEYS.PACKAGES_INDEX, '0');
                                            this.state.navigation.navigate('Packages');
                                        }}>
                                            <Ionicons name='ios-analytics' size={32} color='orange' />
                                            <Text style={s.title}>RateList</Text>
                                        </TouchableOpacity>
                                    </Row>
                                </Col>
                                <Col >
                                    <Row style={s.row}>
                                        <TouchableOpacity style={s.rowInner} onPress={() => {
                                            this.state.navigation.navigate('Appointments');
                                        }}>
                                            <Ionicons name='ios-calendar-outline' size={32} color='orange' />
                                            <Text style={s.title}>Home Sampling</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={s.row}>
                                        <TouchableOpacity style={s.rowInner} onPress={() => {
                                            SAVE_TO_STORE(STORE_KEYS.PACKAGES_INDEX, '1');
                                            this.state.navigation.navigate('Packages');
                                        }}>
                                            <Ionicons name='md-pricetags-outline' size={32} color='orange' />
                                            <Text style={s.title}>Packages</Text>
                                        </TouchableOpacity>
                                    </Row>
                                </Col>
                                <Col >
                                    <Row style={[s.row]}>
                                        <TouchableOpacity style={s.rowInner} onPress={() => {
                                            this.state.navigation.navigate('Scanner');
                                        }}>
                                            <Ionicons name='qr-code-outline' size={40} color='orange' />
                                            <Text style={[s.title]}>Scan QR</Text>
                                        </TouchableOpacity>
                                    </Row>
                                </Col>
                            </Grid>
                        </View>


                        <Text style={s.SubTitle}>Connect</Text>


                        <View style={s.contactWrapper}>
                            <Grid>
                                <Col style={[s.contact, { backgroundColor: '#25D366' }]}>
                                    <TouchableOpacity onPress={() => {
                                        Linking.openURL('whatsapp://send?phone=923111555586')
                                            .then(data => {
                                                console.log("WhatsApp Opened successfully " + data);  //<---Success
                                            })
                                            .catch(() => {
                                                Linking.openURL('https://wa.me/923111555586')
                                            });
                                    }}>
                                        <View style={s.inner}>
                                            <Ionicons name='logo-whatsapp' size={32} color='white' />
                                            <Text style={[s.title, s.filledText]}>WhatsApp</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Col>




                                <Col style={[s.contact, { backgroundColor: '#4267B2' }]}>
                                    <TouchableOpacity onPress={() => {
                                        Linking.openURL('https://www.facebook.com/Nxgen-Labs-153377176821731')
                                    }}>
                                        <View style={s.inner} >
                                            <Ionicons name='ios-logo-facebook' size={32} color='white' />
                                            <Text style={[s.title, s.filledText]}>Facebook</Text>
                                        </View>
                                    </TouchableOpacity>
                                </Col>






                                <Col style={[s.contact, { backgroundColor: '#1DA1F2' }]}>
                                    <TouchableOpacity onPress={() => {
                                        Linking.openURL('https://twitter.com/LabsNxgen')
                                    }}>
                                        <View style={s.inner}>
                                            <Ionicons name='logo-twitter' size={32} color='white' />
                                            <Text style={[s.title, s.filledText]}>Twitter</Text>
                                        </View>

                                    </TouchableOpacity>
                                </Col>






                                <Col style={[s.contact, { backgroundColor: '#E1306C' }]}>
                                    <TouchableOpacity onPress={() => {
                                        Linking.openURL('https://www.instagram.com/nxgenlabs')
                                    }}>
                                        <View style={s.inner}>
                                            <Ionicons name='logo-instagram' size={32} color='white' />
                                            <Text style={[s.title, s.filledText]}>Instagram</Text>

                                        </View>
                                    </TouchableOpacity>
                                </Col>



                            </Grid>
                        </View>


                        <StatusBar style="auto" />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


const s = StyleSheet.create({
    container: {
        flex: 1,
        padding: 14,
    },
    logo: {
        width: 200,
        height: 100,
        resizeMode: 'contain',
        margin: -20,
    },
    titleContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#FAFAFA',
        marginBottom: 14,
        shadowColor: '#BDBDBD',
        shadowRadius: 10,
        shadowOpacity: 0.5,
        borderRadius: 10
    },
    pageTitle: {
        fontSize: 30,
        fontWeight: '800'
    },
    imagesContainer: {
        borderRadius: 6,
        backgroundColor: '#FFF3E0',
        overflow: 'hidden',
        marginBottom: 30
    },
    SubTitle: {
        fontSize: 18,
        fontWeight: '700',
    },
    servicesContainer: {
        marginTop: 2,
        height: 225,
        margin: -10,
        marginBottom: 20
    },
    row: {
        margin: 6,
        backgroundColor: 'white',
        borderRadius: 6,
        shadowColor: '#EEEEEE',
        shadowRadius: 4,
        shadowOpacity: 0.5,
    },
    rowInner: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        width: '100%',
        height: '100%'
    },
    icon: {
        width: 30
    },
    title: {
        fontSize: 12,
        color: 'gray',
        marginTop: 10,
        fontWeight: '400'
    },

    contactWrapper: {
        marginTop: 2,
        height: 100,
        margin: -10,
    },
    contact: {
        margin: 6,
        backgroundColor: 'white',
        borderRadius: 6,
        padding: 10,
        shadowColor: '#EEEEEE',
        shadowRadius: 4,
        shadowOpacity: 0.5,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'

    },
    inner: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

    filledText: {
        color: 'white',
        fontSize: 10,
        fontWeight: '500'
    }
});

