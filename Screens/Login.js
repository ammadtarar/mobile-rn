import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { Text, View, Image, ScrollView, Keyboard, Dimensions } from 'react-native';
import { authStyles } from '../styles';
import Button from "../Components/Button";
import TextView from '../Components/TextView';
import { showMessage, hideMessage } from "react-native-flash-message";
import Spinner from 'react-native-loading-spinner-overlay';

import { HTTP, URLS } from '../Controllers/Network';
var qs = require("qs");
import { SAVE_TO_STORE, STORE_KEYS } from '../Controllers/Store';


export default function App({ navigation }) {

    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [showSpinner, setShowSpinner] = useState(false);
    const [height, setHeight] = useState('100%')

    useEffect(() => {
        console.log('sup');
        let ctx = this;
        let ScreenHeight = Dimensions.get("window").height;
        Keyboard.addListener('keyboardDidShow', (e) => {
            console.log('hi');
            console.log(e.endCoordinates.height);
            console.log(ScreenHeight);
            setHeight(ScreenHeight - e.endCoordinates.height)
        });
        Keyboard.addListener('keyboardDidHide', () => {
            setHeight('100%')
        });
    }, [])


    function validateEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function login() {
        if (!phone || phone.length <= 0) {
            showMessage({ type: 'danger', message: 'Please enter a valid email or mobile number' });
            return
        }

        if (!password || password.length <= 0) {
            showMessage({ type: 'danger', message: 'Please enter your password' });
            return
        }

        setShowSpinner(true);


        var data = qs.stringify({
            username: phone,
            password: password,
            type: validateEmail(phone) ? "email" : "phone",
        });

        var config = {
            method: "post",
            url: URLS.baseURL + URLS.AUTH.LOGIN,
            data: data,
        };

        HTTP(config)
            .then(function (response) {
                let data = JSON.parse(JSON.stringify(response.data.patient));
                console.log(data)
                SAVE_TO_STORE(STORE_KEYS.PHONE, data.phone);
                SAVE_TO_STORE(STORE_KEYS.USERNAME, data.phone);
                SAVE_TO_STORE(STORE_KEYS.ID, data.id);
                SAVE_TO_STORE(STORE_KEYS.NAME, data.name);
                SAVE_TO_STORE(STORE_KEYS.TOKEN, data.token);
                SAVE_TO_STORE(STORE_KEYS.EMAIL, data.email);
                setShowSpinner(false)
                goToHome()
            })
            .catch(function (error) {
                console.log(error);
                setShowSpinner(false)
            });

    };

    function goToHome() {
        setShowSpinner(false)
        showMessage({
            message: "Login Successful",
            type: "success",
        });
        navigation.reset({
            index: 0,
            routes: [{ name: 'Home' }],
        });
    }

    return (
        <View style={[{ height: height }]}>

            <Image style={authStyles.bg} source={require('../assets/images/gradient.png')} />

            <ScrollView style={authStyles.sv}>

                <View style={authStyles.container}>
                    <Spinner
                        visible={showSpinner}
                        spinnerTextStyle={{ color: 'white' }}
                    />

                    <Image style={authStyles.logo} source={require('../assets/images/logo2.png')} />
                    <View style={authStyles.card}>
                        <View style={authStyles.cardBg}></View>
                        <Text style={authStyles.login}>Login</Text>
                        <TextView onChangeText={(text) => setPhone(text)} title='Email / Phone' placeholder='Enter your email or mobile number here' />
                        <TextView secureText={true} onChangeText={(text) => setPassword(text)} value={setPassword} type='password' title='Password' placeholder='Enter your password here' />
                        <Button type='big' title='Login' onPress={() => {
                            login();
                        }} />
                        <View style={authStyles.forgotPassword}>
                            <Text style={authStyles.forgotText}>Forget your password ?</Text>
                            <Button type='simple' title='Reset Password' onPress={() => {
                                navigation.navigate('ForgotPassword')
                            }} />
                        </View>
                    </View>
                    <View style={authStyles.notRegistered}>
                        <Text style={authStyles.notRegInfo}>Don't have an account ?</Text>
                        <Button title='Register' type='small' onPress={() => {
                            navigation.navigate('Register')
                        }} />
                    </View>
                    <StatusBar style="dark" />

                </View>
            </ScrollView>




        </View>
    );
}




