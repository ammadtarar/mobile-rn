import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { Text, View, Image, TextInput, ScrollView, TouchableOpacity, StyleSheet, Keyboard, Dimensions } from 'react-native';
import { authStyles } from '../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RadioGroup from 'react-native-radio-buttons-group'
import Button from "../Components/Button";
import TextView from '../Components/TextView';
import { showMessage, hideMessage } from "react-native-flash-message";
import Spinner from 'react-native-loading-spinner-overlay';

import { HTTP, URLS } from '../Controllers/Network';
var qs = require("qs");




const radioButtonsData = [{
    id: '1', // acts as primary key, should be unique and non-empty string
    label: 'Male',
    value: 'male',
    size: 18
}, {
    id: '2',
    label: 'Female',
    value: 'female',
    size: 18
}, {
    id: '3',
    label: 'Others',
    value: 'others',
    size: 18
}]


const regTypes = [{
    id: '1', // acts as primary key, should be unique and non-empty string
    label: 'Phone',
    value: 'phone',
    size: 18,
    selected: true
}, {
    id: '2',
    label: 'Email',
    value: 'email',
    size: 18
}]


export default function App({ navigation }) {

    const [step, setStep] = useState(1);
    const [showSpinner, setShowSpinner] = useState(false);
    const [radioButtons, setRadioButtons] = useState(radioButtonsData);
    const [btnTitle, setBtnTitle] = useState('Send OTP Code')
    const [message, setMessage] = useState('Please select a registration mode. You can either choose to register using your phone number or your email address')

    const [registrationType, setRegistrationType] = useState('phone');
    const [destination, setDestination] = useState('');
    const [otp, setOtp] = useState('');

    const [password, setPassword] = useState('');
    const [repeatPassword, setRepeatPassword] = useState('');

    const [emailOrPhone, setEmailOrPhone] = useState('');
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [gender, setGender] = useState('');

    const [height, setHeight] = useState('100%')

    useEffect(() => {
        console.log('sup');
        let ctx = this;
        let ScreenHeight = Dimensions.get("window").height;
        Keyboard.addListener('keyboardDidShow', (e) => {
            console.log('hi');
            console.log(e.endCoordinates.height);
            console.log(ScreenHeight);
            setHeight(ScreenHeight - e.endCoordinates.height)
        });
        Keyboard.addListener('keyboardDidHide', () => {
            setHeight('100%')
        });
    }, [])

    function onPressRadioButton(radioButtonsArray) {
        radioButtonsArray.forEach(element => {
            if (element.selected) {
                setGender(element.value)
                return
            }
        });
    }

    function onPressRegType(arr) {
        regTypes.forEach(element => {
            if (element.selected) {
                setRegistrationType(element.value)
                return
            }
        });
    }

    function onClickMainButton() {
        if (step == 1) {
            requestOtp(false)
        } else if (step == 2) {
            verifyOtp();
        } else if (step == 3) {
            verifyPassword();
        } else {
            register();
        }
    }

    function sendOtpAgain() {
        console.log('sendOtpAgain');
        requestOtp(true)
    }

    function requestOtp(requestingAgain) {
        if (!destination || destination.length <= 0) {
            showMessage({ type: 'danger', message: `Please enter a valid ${destination}` });
            return
        }
        setShowSpinner(true)
        var url = URLS.AUTH.OTP.replace(':dest', destination);
        url = url.replace(":type", registrationType);
        HTTP
            .patch(url)
            .then((res) => {
                console.log('OTP RESPONSE');
                console.log(res.data.code);
                setShowSpinner(false)
                showMessage({ type: 'success', message: 'OTP sent successfully' });
                if (!requestingAgain) {
                    setStep(2)
                    setMessage('Please enter the OTP code that we sent you for verification');
                    setBtnTitle('Verify')
                }
            })
            .catch((err) => {
                setShowSpinner(false)
                console.log("register err");
                console.log(err);
            });
    }

    function verifyOtp() {
        if (!destination || destination.length <= 0) {
            showMessage({ type: 'danger', message: `Please enter a valid ${destination}` });
            return
        }
        if (!otp || otp.length <= 0) {
            showMessage({ type: 'danger', message: `Please enter the OTP code` });
            return
        }
        setShowSpinner(true)
        var url = URLS.AUTH.VERIFY.replace(':dest', destination);
        url = url.replace(":code", otp);
        HTTP
            .get(url)
            .then((res) => {
                console.log('OTP VERIFY RESPONSE');
                console.log(res.data);
                setShowSpinner(false)
                showMessage({ type: 'success', message: 'OTP verification successful' });
                setStep(3)
                setMessage('Thanks for the verification. Now please enter a password');
                setBtnTitle('Save Password')
            })
            .catch((err) => {
                setShowSpinner(false)
                console.log("register err");
                console.log(err);
            });

    }

    function verifyPassword() {
        if (!password || password.length <= 7) {
            showMessage({ type: 'danger', message: 'Please enter a valid password. Password lenght should be greater then 7' });
            return
        }
        if (!repeatPassword || repeatPassword.length <= 7) {
            showMessage({ type: 'danger', message: 'Please enter the password again. Password lenght should be greater then 7' });
            return
        }
        if (password !== repeatPassword) {
            showMessage({ type: 'danger', message: 'Passwords do no match. Please make sure the passwords match' });
        }
        setStep(4)
        setMessage('Great. We are almost there , please tell us a little about yourself');
        setBtnTitle('Finish')
    }

    function register() {
        if (!name || name.length <= 0) {
            showMessage({ type: 'danger', message: 'Please enter your fullname' });
            return
        }
        if (!gender || gender.length <= 0) {
            showMessage({ type: 'danger', message: 'Please select a gender' });
            return
        }

        if (!address || address.length <= 0) {
            showMessage({ type: 'danger', message: 'Please enter your full address' });
            return
        }

        if (!city || city.length <= 0) {
            showMessage({ type: 'danger', message: 'Please enter your city name' });
            return
        }



        setShowSpinner(true)

        HTTP
            .post(URLS.AUTH.REGISTER, {
                [registrationType]: destination,
                [registrationType == 'phone' ? 'email' : 'phone']: emailOrPhone,
                name: name,
                password: repeatPassword,
                gender: gender,
                address: address,
                city: city
            })
            .then((res) => {
                setShowSpinner(false)
                showMessage({ type: 'success', message: 'Congratulations, your account has been created' });
                let data = JSON.parse(JSON.stringify(res.data));
                console.log("data");
                console.log(data);
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }],
                })
            })
            .catch((err) => {
                setShowSpinner(false)
                console.log("register err");
                console.log(err);
            });
    }



    return (

        <View style={[{ height: height }]}>
            <Image style={authStyles.bg} source={require('../assets/images/gradient.png')} />
            <ScrollView style={authStyles.sv}>
                <View style={authStyles.container}>
                    <Spinner
                        visible={showSpinner}
                        textContent={'Please Wait...'}
                        spinnerTextStyle={{ color: 'white' }}
                    />
                    <TouchableOpacity style={authStyles.back} onPress={() => {
                        navigation.goBack()
                    }}>
                        {<Ionicons size={30} name='ios-close' color='gray' />}
                    </TouchableOpacity>
                    <Image style={authStyles.logo} source={require('../assets/images/logo2.png')} />
                    <View style={authStyles.card}>
                        <View style={authStyles.cardBg}></View>
                        <Text style={authStyles.login}>Register ({step}/4)</Text>
                        <Text style={authStyles.info}>
                            {message}
                        </Text>

                        {
                            step == 1 || step == 2 ?
                                <View>

                                    <RadioGroup
                                        containerStyle={{ marginBottom: 20, marginTop: 0 }}
                                        layout='row'
                                        radioButtons={regTypes}
                                        onPress={onPressRegType}
                                    />
                                    <TextView
                                        value={{ destination }}
                                        onChangeText={(text) => setDestination(text)}
                                        title={registrationType == 'phone' ? 'Phone' : 'Email'}
                                        placeholder={`Enter your ${registrationType} here`}
                                    />

                                </View>
                                :
                                <View>
                                </View>

                        }
                        {
                            step == 2 ?
                                <View style={s.codeContainer}>
                                    <View style={s.codeContainerChild}>
                                        <TextView value={{ otp }} onChangeText={(text) => setOtp(text)} title='OTP' placeholder='Enter the OTP code here' />
                                    </View>
                                    <TouchableOpacity style={[s.codeContainerChild, s.btOtpContainer]} onPress={sendOtpAgain}>
                                        <Text style={s.sendAgain}>
                                            Send Again
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View></View>
                        }


                        {
                            step == 3 ?
                                <View >
                                    <TextView
                                        value={{ password }}
                                        secureText={true}
                                        onChangeText={(text) => setPassword(text)}
                                        title='Password'
                                        placeholder='Enter your password here'
                                    />
                                    <TextView
                                        value={{ repeatPassword }}
                                        secureText={true} onChangeText={(text) => setRepeatPassword(text)}
                                        title='Confirm Password'
                                        placeholder='Enter your password again here'
                                    />
                                </View>
                                :
                                <View></View>
                        }


                        {
                            step == 4 ?
                                <View >
                                    <TextView
                                        value={{ emailOrPhone }}
                                        onChangeText={(text) => setEmailOrPhone(text)}
                                        title={registrationType == 'phone' ? 'Email (Optional)' : 'Phone (Optional)'}
                                        placeholder={registrationType == 'phone' ? 'Please enter your email' : 'Please enter your phone'}
                                    />
                                    <TextView
                                        value={{ name }}
                                        onChangeText={(text) => setName(text)}
                                        title='Name'
                                        placeholder='Enter your name here'
                                    />
                                    <RadioGroup
                                        containerStyle={{ marginBottom: 20, marginTop: 5 }}
                                        layout='row'
                                        radioButtons={radioButtons}
                                        onPress={onPressRadioButton}
                                    />
                                    <TextView
                                        value={{ address }}
                                        onChangeText={(text) => setAddress(text)}
                                        title='Address'
                                        placeholder='Enter your full address here'
                                    />
                                    <TextView
                                        value={{ city }}
                                        onChangeText={(text) => setCity(text)}
                                        title='City'
                                        placeholder='Enter your city name here'
                                    />

                                </View>
                                :
                                <View></View>
                        }


                        <Button type='big' title={btnTitle} onPress={onClickMainButton} />

                    </View>
                    <View style={[authStyles.notRegistered, authStyles.spacing]} >
                        <Text style={authStyles.notRegInfo}>Already have an account ?</Text>
                        <Button type='small' title='Login' onPress={() => {
                            navigation.goBack()
                        }} />

                    </View>
                    <StatusBar style="dark" />
                </View>
            </ScrollView>
        </View>

    );
}

const s = StyleSheet.create({
    codeContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        height: 60,
        marginBottom: 10
    },
    codeContainerChild: {
        width: '58%'
    },
    btOtpContainer: {
        width: "40%",
        marginLeft: '2%',
        display: 'flex'
    },
    sendAgain: {
        width: '100%',
        height: 40,
        marginTop: 'auto',
        marginBottom: 2,
        borderRadius: 4,
        overflow: 'hidden',
        textAlign: 'center',
        lineHeight: 40,
        color: 'orange',
        borderColor: 'orange',
        borderWidth: 1
    }
})