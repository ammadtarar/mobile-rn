import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, StyleSheet, Keyboard, Dimensions } from 'react-native';
import { authStyles } from '../styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from "../Components/Button";
import TextView from '../Components/TextView';
import { showMessage, hideMessage } from "react-native-flash-message";
import Spinner from 'react-native-loading-spinner-overlay';
import { HTTP, URLS } from '../Controllers/Network';

export default function App({ navigation }) {

    const [showSpinner, setShowSpinner] = useState(false);
    const [email, setEmail] = useState('');
    const [codeSent, setCodeSent] = useState(false);
    const [otp, setOTP] = useState('');
    const [password, setPassword] = useState('');
    const [repeatPassword, setRepeatPassword] = useState('');
    const [height, setHeight] = useState('100%')

    useEffect(() => {
        console.log('sup');
        let ctx = this;
        let ScreenHeight = Dimensions.get("window").height;
        Keyboard.addListener('keyboardDidShow', (e) => {
            console.log('hi');
            console.log(e.endCoordinates.height);
            console.log(ScreenHeight);
            setHeight(ScreenHeight - e.endCoordinates.height)
        });
        Keyboard.addListener('keyboardDidHide', () => {
            setHeight('100%')
        });
    }, [])


    function onClickReset() {
        console.log("sup");
        if (!codeSent) {
            if (!email || email.length <= 0) {
                showMessage({ type: 'danger', message: 'Please enter a valid email address' })
                return
            }
            setShowSpinner(true);
            requestOTP();
        } else {
            resetPassword();
        }
    }

    function validateEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function requestOTP() {
        var url = URLS.AUTH.OTP.replace(':dest', email)
        url = url.replace(':type', validateEmail(email) ? 'email' : 'phone')
        console.log(url);
        HTTP.patch(url)
            .then((r) => {
                console.log(JSON.parse(JSON.stringify(r)));
                setCodeSent(true)
                setShowSpinner(false);
                showMessage({ type: 'success', message: 'OTP emailed successfully. Please check your inbox' })
            })
            .catch(err => {
                setCodeSent(false)
                setShowSpinner(false);
                showMessage({ type: 'danger', message: err.message || 'Failed to request OTP. Please try again later' })
            })
    }

    function resetPassword() {
        if (!otp) {
            showMessage({ type: 'danger', message: 'Please enter OTP code' })
            return
        }

        if (!password) {
            showMessage({ type: 'danger', message: 'Please enter your new password' })
            return
        }

        if (!repeatPassword) {
            showMessage({ type: 'danger', message: 'Please enter your new password again' })
            return
        }

        if (password != repeatPassword) {
            showMessage({ type: 'danger', message: 'Passwords do not match. Please make sure both password are similar' })
            return
        }

        setShowSpinner(true);

        HTTP.post(URLS.AUTH.RESET_PASSWORD, {
            destination: email,
            password: password,
            otp: otp,
            type: validateEmail(email) ? 'email' : 'phone'
        })
            .then(res => {
                console.log(res);
                setShowSpinner(false);
                showMessage({ type: 'success', message: 'Password reset successfully' })
                navigation.goBack()
            })
            .catch(err => {
                setShowSpinner(false);
                showMessage({ type: 'danger', message: err.message || 'Failed to reset password. Please make sure email , password and otp are correct' })
            })
    }


    return (
        <View style={[{ height: height }]}>
            <Image style={authStyles.bg} source={require('../assets/images/gradient.png')} />
            <ScrollView style={authStyles.sv}>
                <View style={authStyles.container}>
                    <Spinner
                        visible={showSpinner}
                        textContent={'Please Wait...'}
                        spinnerTextStyle={{ color: 'white' }}
                    />
                    <TouchableOpacity style={authStyles.back} onPress={() => {
                        navigation.goBack()
                    }}>
                        {<Ionicons size='30' name='ios-close' color='gray' />}
                    </TouchableOpacity>
                    <Image style={authStyles.logo} source={require('../assets/images/logo2.png')} />
                    <View style={authStyles.card}>
                        <View style={authStyles.cardBg}></View>
                        <Text style={authStyles.login}>Reset Password</Text>

                        <Text style={authStyles.info}>
                            Please enter your phone number or email address below. We will send you a verification code
                        </Text>
                        <TextView onChangeText={(text) => setEmail(text)} title='Phone / Email' placeholder='Enter your phone or email' />

                        {
                            codeSent ?
                                <View>
                                    <View style={s.otpContainer}>
                                        <View style={s.inner}>
                                            <TextView onChangeText={(text) => setOTP(text)} title='OTP' placeholder='OTP code' />
                                        </View>
                                        <View style={s.inner}>
                                            <TouchableOpacity onPress={requestOTP}>
                                                <View style={s.bt}>
                                                    <Text style={s.btText}>Send Again</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                    <TextView onChangeText={(text) => setPassword(text)} secureText={true} type='password' title='New Password' placeholder='Enter new password' />
                                    <TextView onChangeText={(text) => setRepeatPassword(text)} secureText={true} type='password' title='Repeat New Password' placeholder='Please enter new password again' />


                                </View> :
                                <View></View>
                        }

                        <Button title={codeSent ? 'Reset Password' : 'Request OTP'} type='big' onPress={onClickReset}></Button>
                    </View>
                    <StatusBar style="dark" />
                </View>
            </ScrollView>
        </View>

    );
}


const s = StyleSheet.create({
    otpContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    inner: {
        width: '50%',
    },
    bt: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 10,
        borderRadius: 4,
        backgroundColor: 'orange'
    },
    btText: {
        color: 'white',
        fontSize: 14,
    }

});



