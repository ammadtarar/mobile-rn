import * as SecureStore from 'expo-secure-store'

const STORE_KEYS = {
    ID: 'id',
    NAME: 'name',
    PHONE: 'phone',
    EMAIL: 'email',
    TOKEN: 'token',
    USERNAME: 'username',
    PACKAGES_INDEX: 'packages_index'
}

const SAVE_TO_STORE = (key, value) => {
    return SecureStore.setItemAsync(key, value)
}

const GET_FROM_STORE = (key) => {
    return SecureStore.getItemAsync(key)
}

const CLEAR_STORE = () => {
    return new Promise(async (resolve, reject) => {
        console.log('starting');
        await SecureStore.deleteItemAsync(STORE_KEYS.TOKEN);
        await SecureStore.deleteItemAsync(STORE_KEYS.ID);
        await SecureStore.deleteItemAsync(STORE_KEYS.NAME);
        await SecureStore.deleteItemAsync(STORE_KEYS.EMAIL);
        console.log('end');
        resolve();

    })

}

export { SAVE_TO_STORE, GET_FROM_STORE, STORE_KEYS, CLEAR_STORE }