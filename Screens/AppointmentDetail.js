import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import { Text, View, StyleSheet, Button, FlatList, SafeAreaView, ScrollView, Linking, RefreshControl } from 'react-native';
import Moment from 'moment';
import { HTTP, URLS } from '../Controllers/Network';
import { GET_FROM_STORE, STORE_KEYS } from '../Controllers/Store';
export default function App({ route, navigation }) {

    const [appData, setAppData] = useState({})
    const [refreshing, setRefreshing] = useState(false);

    const { appointment } = route.params
    useEffect(() => {
        console.log(appointment);
        setAppData(appointment)
    }, []);

    function onRefresh() {
        console.log('REFRESGING');
        setRefreshing(true)
        GET_FROM_STORE(STORE_KEYS.TOKEN)
            .then(token => {
                HTTP
                    .get(URLS.APPOINTMENT.BY_ID.replace(":id", appData.id), {
                        headers: {
                            'Authorization': token
                        }
                    })
                    .then((res) => {
                        let data = res.data
                        console.log('DATA');
                        console.log(data);
                        setAppData(data)
                        setRefreshing(false)
                    })
                    .catch((err) => {
                        console.log('== ERROR');
                        console.log(err);
                        setRefreshing(false)
                    });
            })

    }


    const renderItem = ({ item }) => (

        <View style={s.item}>
            <View style={[s.testKv]}>
                <Text style={s.testV}>{item.name}</Text>
                {
                    item.status != 'completed' ?
                        <Text style={s.testV}>{item.status.replace('_', ' ')}</Text> :
                        <TouchableOpacity onPress={() => {
                            let url = `http://203.124.51.250:1244/lims/offlineReports/normaltestreport.aspx?format=${item.format}&computerno=${appointment.lims_computerno}&testdetailcode=${item.test_detail_code}&pageno=582GjeW21&checkpage=6`;
                            Linking.openURL(url)
                        }}>
                            <Text style={s.bt}>Download Report</Text>
                        </TouchableOpacity>
                }

            </View>


        </View >
    );

    return (
        <SafeAreaView style={s.container}>

            <ScrollView style={s.wrapper} refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    enabled={true}
                    onRefresh={onRefresh}
                />
            }>
                <View style={s.card}>
                    <Text style={s.cardTitle}>Patient Details</Text>
                    <View style={s.line}></View>
                    <View style={s.cardInfo}>
                        <View style={s.kv}>
                            <Text style={s.k}>Name</Text>
                            <Text style={s.invfo}>{appData.name || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Gender</Text>
                            <Text style={[s.invfo, s.cap]}>{appData.gender || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Phone</Text>
                            <Text style={s.invfo}>{appData.phone || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Email</Text>
                            <Text style={s.invfo}>{appData.email || '--'}</Text>
                        </View>
                    </View>
                </View>


                <View style={s.card}>
                    <Text style={s.cardTitle}>Appointment Details</Text>
                    <View style={s.line}></View>
                    <View style={s.cardInfo}>
                        <View style={s.kv}>
                            <Text style={s.k}>ID</Text>
                            <Text style={s.invfo}>{appData.id || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Mode</Text>
                            <Text style={[s.invfo, s.cap]}>{appData.mode ? appData.mode.replace('_', ' ') : '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Address</Text>
                            <Text style={s.invfo}>{appData.address || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>City</Text>
                            <Text style={s.invfo}>{appData.city || '--'}</Text>
                        </View>
                        <View style={s.kv}>
                            <Text style={s.k}>Time</Text>
                            <Text style={s.invfo}>{Moment(appData.time).format('DD/MM/YYYY HH:mm') || '--'}</Text>
                        </View>
                    </View>
                </View>



                <View style={s.card}>
                    <Text style={s.cardTitle}>Tests Details</Text>
                    <View style={s.line}></View>
                    <View style={[s.testsHeader]}>
                        <Text style={s.testHeaderK}>Name</Text>
                        <Text style={s.testHeaderK}>Status</Text>
                    </View>

                    <FlatList
                        data={appData.tests}
                        renderItem={renderItem}
                        key={item => item.id}
                    />


                </View>



            </ScrollView>



        </SafeAreaView>
    );
}


const s = StyleSheet.create({
    container: {
        flex: 1,
    },
    wrapper: {
        overflow: 'scroll',
        padding: 10
    },
    card: {
        width: '100%',
        backgroundColor: 'white',
        marginBottom: 20,
        borderRadius: 8,
        shadowColor: 'gray',
        shadowRadius: 10,
        shadowOpacity: 0.2,
        overflow: 'hidden'
    },
    cardTitle: {
        fontSize: 18,
        fontWeight: 'bold',

        color: 'orange',
        padding: 20,

    },
    cardInfo: {
        display: 'flex',
        flexDirection: 'column',
        padding: 20,
    },
    line: {
        width: '100%',
        height: 1,
        backgroundColor: 'orange'
    },
    kv: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 10,
        overflow: 'hidden'
    },
    k: {
        fontSize: 13,
        color: 'gray',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        width: 100
    },
    v: {
        fontSize: 16,
        color: 'black',
    },
    testsHeader: {
        backgroundColor: '#FFF3E0',
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        paddingTop: 10,
        flexDirection: 'row'
    },
    testHeaderK: {
        width: '50%',
        fontSize: 14,
        color: 'gray'
    },
    testKv: {
        width: '100%',
        padding: 14,
        flexDirection: 'row'
    },
    testV: {
        width: '50%',
        textTransform: 'capitalize'
    },
    bt: {
        fontWeight: 'bold',
        backgroundColor: 'orange',
        color: 'white',
        padding: 4,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 4,
        overflow: 'hidden'
    },
    cap: { textTransform: 'capitalize' }
})