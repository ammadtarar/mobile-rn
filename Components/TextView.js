import React, { useEffect } from 'react';
import { Text, View, TextInput, KeyboardAvoidingView } from 'react-native';
import { authStyles } from "../styles";



const TextView = (props) => {

    // useEffect(() => {
    //     console.log('sup');
    //     console.log(props);
    // })


    return (

        <View style={props.type == 'search' ? authStyles.searchBox : ''}>


            <Text displ style={props.type == 'search' ? authStyles.searchTitle : authStyles.field}>{props.title}</Text>


            <TextInput
                value={props.val}
                secureTextEntry={props.secureText || false}
                onChangeText={props.onChangeText}
                style={props.type == 'search' ? authStyles.searchInput : authStyles.input}
                placeholder={props.placeholder}
                type={props.type || 'text'}
                autoCapitalize='none'
            />




        </View>
    );
}


export default TextView;


