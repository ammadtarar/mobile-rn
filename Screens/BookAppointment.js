import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, Keyboard, Dimensions, TouchableOpacity, } from 'react-native';
import TextView from '../Components/TextView';
import RadioGroup from 'react-native-radio-buttons-group';
import { authStyles } from "../styles";
import Button from '../Components/Button';
import { HTTP, URLS } from '../Controllers/Network';


import SelectBox from 'react-native-multi-selectbox'
import _, { xorBy } from 'lodash'

import { showMessage, hideMessage } from "react-native-flash-message";
import { GET_FROM_STORE, STORE_KEYS } from '../Controllers/Store';


import Moment from 'moment';
import DateTimePickerModal from "react-native-modal-datetime-picker";

export default class App extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            showDobPicker: false,
            showTimePicker: false,
            height: '100%',
            appointments: [],
            navigation: props.navigation,
            genders: [{
                id: '1',
                label: 'Male',
                value: 'male',
                size: 18
            }, {
                id: '2',
                label: 'Female',
                value: 'female',
                size: 18
            }, {
                id: '3',
                label: 'Other',
                value: 'other',
                size: 18
            }],
            modes: [{
                id: '1',
                value: 'home_sampling',
                label: 'Home Pickup',
                size: 18
            }, {
                id: '2',
                value: 'visit_lab',
                label: 'Visit Lab',
                size: 18
            }],
            dob: new Date(),
            name: '',
            phone: '',
            email: '',
            address: '',
            city: '',
            labTests: [],
            selectedTeams: [],
            date: new Date(),
            communications: [{
                id: '1',
                value: 'phone',
                label: 'Phone',
                size: 18,
                selected: true
            }, {
                id: '2',
                value: 'email',
                label: 'Email',
                size: 18
            }],
            communication: 'phone',

        }

        props.navigation.addListener('focus', () => {
            this.getRateList();
        });

    }

    componentWillMount() {
        let ctx = this;
        let ScreenHeight = Dimensions.get("window").height;
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => {
            console.log('hi');
            console.log(e.endCoordinates.height);
            console.log(ScreenHeight);
            ctx.setState({ height: ScreenHeight - e.endCoordinates.height - 70 })
        });
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
            ctx.setState({ height: '100%' })
        });
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow() {
        console.log('SUP');
        this.setState({ height: 200 })
    }

    keyboardDidHide() {
        this.setState({ height: '100%' })
    }






    getRateList() {
        HTTP
            .get(URLS.RATELIST.TESTS)
            .then((res) => {
                let data = res.data;
                var parsed = [];
                data.forEach(e => {
                    parsed.push({
                        item: e.name,
                        id: e.id
                    });
                })
                this.setState({ labTests: parsed })
            })
            .catch((err) => {
                console.log('== ERROR');
                console.log(err);
            });
    }


    onMultiChange() {
        return (item) => this.setState({ selectedTeams: xorBy(this.state.selectedTeams, [item], 'id') })
    }






    render() {
        return (


            <View style={[s.container, { height: this.state.height }]} >
                <ScrollView style={s.sv}>
                    <Text style={s.info}>Enter your details to book appointment for medical test , either for home sample pickup or on location testing</Text>
                    <TextView onChangeText={(text) => this.setState({ name: text })} title='Name' placeholder='Your full name' />
                    <Text style={authStyles.field}>Gender</Text>
                    <RadioGroup
                        containerStyle={{ marginBottom: 20, marginTop: 5 }}
                        layout='row'
                        radioButtons={this.state.genders}
                        onPress={(r) => {
                            r.forEach(element => {
                                if (element.selected) {
                                    this.setState({ gender: element.value })
                                    return
                                }
                            });
                        }}
                    />

                    <View style={{ marginBottom: 20 }}>


                        <TouchableOpacity onPress={() => { this.setState({ showDobPicker: true }) }}>
                            <Text style={authStyles.field}>Date Of Birth</Text>
                            <View style={s.dateWrapper}>
                                <Text style={[s.date, { color: typeof this.state.dob == 'string' ? 'lightgray' : 'black' }]}>{typeof this.state.dob == 'string' ? this.state.dob : Moment(this.state.dob).format('DD/MM/YYYY')}</Text>
                            </View>
                        </TouchableOpacity>
                        <DateTimePickerModal
                            isVisible={this.state.showDobPicker}
                            mode="date"
                            date={this.state.dob}
                            onCancel={() => { this.setState({ showDobPicker: false }) }}
                            onConfirm={(time) => { this.setState({ showDobPicker: false, dob: time }) }}
                        />



                    </View>


                    <Text style={authStyles.field}>Preffered way of communication </Text>
                    <RadioGroup
                        containerStyle={{ marginBottom: 20, marginTop: 5 }}
                        layout='row'
                        radioButtons={this.state.communications}
                        onPress={(r) => {
                            r.forEach(element => {
                                if (element.selected) {
                                    this.setState({ communication: element.value })
                                    return
                                }
                            });
                        }}
                    />

                    {
                        this.state.communication == 'phone' ?
                            <TextView onChangeText={(text) => this.setState({ phone: text })} title='Phone' placeholder='Your phone number' /> :
                            <TextView onChangeText={(text) => this.setState({ email: text })} title='Email' type="email" placeholder='Your email address' />
                    }





                    <View style={s.testsWrapper} >

                        <Text style={authStyles.field}>Lab Tests</Text>
                        <View style={s.selectBox}>


                            <SelectBox
                                arrowIconColor='#FFA726'
                                searchIconColor='#FFA726'
                                toggleIconColor="#FFA726"
                                label=""
                                options={this.state.labTests}
                                selectedValues={this.state.selectedTeams}
                                onMultiSelect={(item) => this.setState({ selectedTeams: xorBy(this.state.selectedTeams, [item], 'id') })}
                                onTapClose={(item) => this.setState({ selectedTeams: xorBy(this.state.selectedTeams, [item], 'id') })}
                                isMulti
                                optionsLabelStyle={{
                                    color: 'black',
                                    fontSize: 14
                                }}
                                multiOptionContainerStyle={{
                                    backgroundColor: 'orange',
                                    color: 'white',
                                    fontSize: 16
                                }}
                                multiOptionsLabelStyle={{
                                    fontSize: 12
                                }}
                                inputFilterContainerStyle={{
                                    backgroundColor: 'red',
                                    paddingLeft: 10,
                                    backgroundColor: '#F5F5F5',

                                }}
                                listEmptyLabelStyle={{
                                    color: 'orange',
                                    fontSize: 14,
                                    padding: 10
                                }}
                                inputPlaceholder='Serach for a test name'
                                listEmptyText="no test found "

                            />
                        </View>

                    </View>



                    <Text style={authStyles.field}>MODE </Text>
                    <RadioGroup
                        containerStyle={{ marginBottom: 20, marginTop: 5 }}
                        layout='row'
                        radioButtons={this.state.modes}
                        onPress={(r) => {
                            r.forEach(element => {
                                if (element.selected) {
                                    this.setState({ mode: element.value })
                                    return
                                }
                            });
                        }}
                    />

                    {
                        this.state.mode === 'home_sampling' ?
                            <View>
                                <TextView onChangeText={(text) => this.setState({ address: text })} title='Address' placeholder='Your full address' />
                                <TextView onChangeText={(text) => this.setState({ city: text })} title='City' placeholder='Your city name' />
                            </View> :
                            <View></View>
                    }




                    <TouchableOpacity onPress={() => { this.setState({ showTimePicker: true }) }} style={{ marginBottom: 20 }}>
                        <Text style={authStyles.field}>Appointment Date</Text>
                        <View style={s.dateWrapper}>
                            <Text style={[s.date, { color: typeof this.state.date == 'string' ? 'lightgray' : 'black' }]}>{typeof this.state.date == 'string' ? this.state.date : Moment(this.state.date).format('DD/MM/YYYY HH:mm a')}</Text>
                        </View>
                    </TouchableOpacity>
                    <DateTimePickerModal
                        isVisible={this.state.showTimePicker}
                        mode="datetime"
                        date={this.state.date}
                        onCancel={() => { this.setState({ showTimePicker: false }) }}
                        onConfirm={(time) => { this.setState({ showTimePicker: false, date: time }); console.log('hello'); console.log(time) }}
                    />



                    <Button title='Book Appointment' type='big' onPress={() => {

                        if (!this.state.name || this.state.name == '') {
                            console.log('sp');
                            showMessage({
                                message: 'Please enter your name',
                                type: "danger",
                            });
                            return;
                        }

                        let data = {
                            name: this.state.name
                        }

                        if (!this.state.gender || this.state.gender == '') {
                            showMessage({
                                message: 'Please select a gender',
                                type: "danger",
                            });
                            return;
                        }
                        data.gender = this.state.gender;



                        if (this.state.communication == 'phone' && (!this.state.phone || this.state.phone == '')) {
                            showMessage({
                                message: 'Please enter your phone number',
                                type: "danger",
                            });
                            return;
                        }
                        data.phone = this.state.phone;

                        if (this.state.communication == 'email' && (!this.state.email || this.state.email == '')) {
                            showMessage({
                                message: 'Please enter your email address',
                                type: "danger",
                            });
                            return;
                        }
                        data.email = this.state.email == '' ? null : this.state.email;


                        var selectedTests = [];
                        if (this.state.selectedTeams.length <= 0) {
                            console.log('sup');
                            showMessage({
                                message: 'Please select atleast one lab test',
                                type: "danger",
                            });
                            return;
                        }

                        this.state.selectedTeams.forEach(item => {
                            selectedTests.push({
                                id: item.id,
                                name: item.item
                            })
                        })
                        data.lab_test_name = selectedTests;


                        if (!this.state.mode || this.state.mode == '') {
                            showMessage({
                                message: 'Please select a mode',
                                type: "danger",
                            });
                            return;
                        }
                        data.mode = this.state.mode;

                        if (this.state.mode == 'home_sampling' && (!this.state.address || this.state.address == '')) {
                            showMessage({
                                message: 'Please enter your address',
                                type: "danger",
                            });
                            return;
                        }
                        data.address = this.state.address;

                        if (this.state.mode == 'home_sampling' && (!this.state.city || this.state.city == '')) {
                            showMessage({
                                message: 'Please enter your city name',
                                type: "danger",
                            });
                            return;
                        }
                        data.city = this.state.city;


                        if (!this.state.date || this.state.date == '') {
                            showMessage({
                                message: 'Please select an appointment date',
                                type: "danger",
                            });
                            return;
                        }
                        data.time = this.state.date





                        if (!this.state.dob || this.state.dob == '') {
                            showMessage({
                                message: 'Please enter your date of birth',
                                type: "danger",
                            });
                            return;
                        }
                        data.dob = this.state.dob;




                        GET_FROM_STORE(STORE_KEYS.TOKEN)
                            .then(token => {
                                HTTP
                                    .post(URLS.APPOINTMENT.SAVE, data, {
                                        headers: {
                                            'Authorization': token
                                        }
                                    })
                                    .then((res) => {
                                        let data = res.data
                                        console.log('APPOINTMENT CREATE SUCCESS');
                                        console.log(data);
                                        showMessage({
                                            message: data.message,
                                            type: "success",
                                        });
                                        this.state.navigation.goBack();

                                    })
                                    .catch((err) => {
                                        console.log('== MAKE APPOINTMENT ERROR');
                                        console.log(err);
                                    });
                            })
                    }}></Button>

                </ScrollView>
                <StatusBar style="auto" />
            </View >

        );
    }
}

const s = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 14
    },
    info: {
        fontSize: 12,
        marginBottom: 20
    },
    sv: {
        display: 'flex',
        flexDirection: 'column'
    },
    testsWrapper: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: 30
    },
    selectBox: {
        width: '100%',
        marginTop: -10

    },
    popup: {
        width: '100%',
        height: '100%',
    },
    dateWrapper: {
        width: '100%',
    },
    date: {
        fontSize: 14,
        borderWidth: 0.5,
        borderColor: 'gray',
        padding: 6,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 6
    },
});
