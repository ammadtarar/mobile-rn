import { showMessage, hideMessage } from "react-native-flash-message";
import { GET_FROM_STORE, STORE_KEYS } from "../Controllers/Store";

import axios from "axios";
const baseURL = "http://localhost:2400/";
// const baseURL = "https://api.nxgen.internal.sawatechnologies.org/";

axios.defaults.baseURL = baseURL;

const HTTP = axios.create({
    baseURL: process.env.VUE_APP_API,
    responseType: "json"
});

HTTP.interceptors.request.use(
    function (config) {
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

// HTTP.interceptors.request.use(request => {
//     console.log('Starting Request', JSON.stringify(request, null, 2))
//     return request
// })

// HTTP.interceptors.response.use(response => {
//     console.log('Response:', JSON.stringify(response, null, 2))
//     return response
// })

HTTP.interceptors.response.use(
    (response) => {
        // Notifications.hideLoading();
        return response;
    },
    (error) => {
        console.log('ERROR');
        console.log(error);
        if (error.response && error.response.data) {

            showMessage({
                message: error.response.data.message,
                type: "danger",
            });
            throw error.response.data;
        } else {
            showMessage({
                message: error.message,
                type: "danger",
            });
            throw error.message || 'Something went wrong';
        }

    }
);

const URLS = {
    baseURL: baseURL,
    AUTH: {
        REGISTER: "patient/register",
        LOGIN: "patient/login",
        RESET_PASSWORD: 'patient/reset/password',
        OTP: "patient/send/code?destination=:dest&type=:type",
        VERIFY: "patient/verify/otp?code=:code&destination=:dest"
    },
    RATE_LIST: 'api/services-list',
    PACKAGES_LIST: 'api/services-profile',
    APPOINTMENT: {
        SAVE: 'appointment/create',
        BY_ID: 'appointment/:id',
        LIST: 'appointment/list'
    },
    RATELIST: {
        TESTS: 'ratelist/tests'
    },
    BANNER: {
        LIST: 'banner/list'
    },


};

export { HTTP, URLS };
