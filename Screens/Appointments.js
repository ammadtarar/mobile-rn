import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, SafeAreaView, Image, ActivityIndicator } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { HTTP, URLS } from '../Controllers/Network';
import { GET_FROM_STORE, STORE_KEYS } from '../Controllers/Store';
import Moment from 'moment';
import { showMessage, hideMessage } from "react-native-flash-message";

export default class Appointments extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            navigation: props.navigation,
            isFetching: true,
            spinner: false
        }
        this.getAppointments();
        props.navigation.addListener('focus', () => {
            this.getAppointments();
        });

    }

    getAppointments() {
        this.setState({
            isFetching: true
        })
        GET_FROM_STORE(STORE_KEYS.TOKEN)
            .then(token => {
                HTTP
                    .get(URLS.APPOINTMENT.LIST, {
                        headers: {
                            'Authorization': token
                        }
                    })
                    .then((res) => {
                        let data = res.data.rows
                        this.setState({
                            appointments: data,
                            isFetching: false
                        })

                    })
                    .catch((err) => {
                        console.log('== ERROR');
                        console.log(err);
                        this.setState({
                            isFetching: false
                        })
                    });
            })

    }

    getStatusClass = (status) => {
        var color = ''
        if (status == 'pending') {
            color = 'orange';
        } else if (status == 'completed') {
            color = 'green';
        } else if (status == 'cancelled') {
            color = 'red';
        } else {
            color = 'black';
        }
        return {
            color: color,
            fontWeight: '600'
        };
    }

    getParsedNames = (names) => {
        var n = [];
        names.forEach(item => {
            n.push(item.name)
        })
        return n.toString()
    }

    canCancel(item) {
        if (!item || !item.tests) {
            return false
        }
        return item.tests.every(i => i.status == 'pending');
    }

    setIndexToProcessing(index, val) {
        if (index < 0) {
            return
        }
        let apps = this.state.appointments;
        apps[index].processing = val;
        this.setState({
            appointments: apps
        })
    }

    cancelAppointment(item, index) {
        if (!this.canCancel(item)) {
            showMessage({
                type: 'warning', message: 'This appointment is already completed or is in processing. You cannot cancel this appointment'
            })
            return
        }
        this.setIndexToProcessing(index, true)
        GET_FROM_STORE(STORE_KEYS.TOKEN)
            .then(token => {
                HTTP
                    .delete(URLS.APPOINTMENT.BY_ID.replace(':id', item.id), {
                        headers: {
                            'Authorization': token
                        }
                    })
                    .then((res) => {
                        this.setIndexToProcessing(index, false)
                        showMessage({
                            type: 'success', message: 'Appointment cancelled successfully'
                        })
                        this.getAppointments();
                    })
                    .catch((err) => {
                        this.setIndexToProcessing(index, false)
                        console.log('== ERROR');
                        console.log(err);
                        this.setState({
                            isFetching: false
                        })
                    });
            })

    }

    render() {

        const renderItem = ({ item, index }) => (

            <View style={s.item}>
                <View style={s.top}>
                    <Text style={s.itemTitle}>{item.name}</Text>
                    <View style={s.itemDateWrapper}>
                        <Ionicons size={16} name='ios-calendar-sharp' color='black' />
                        <Text style={s.itemDate}>{Moment(item.time).format('DD/MM/YYYY HH:mm')}</Text>
                    </View>
                </View>


                <View style={s.tests}>
                    <Text style={s.testsTitle}>Tests requested : </Text>
                    <Text style={[s.bottomItemValue]}>{this.getParsedNames(item.tests)}</Text>
                </View>


                <View style={s.tests}>
                    <Text style={s.testsTitle}>Type</Text>
                    <Text style={[s.bottomItemValue]}>{item.mode.replace("_", " ")}</Text>
                </View>

                <View style={s.line}></View>

                <View style={s.btWrapper}>

                    <TouchableOpacity style={s.btInnerWrapper} onPress={() => {
                        this.cancelAppointment(item, index)
                    }}>
                        {
                            !item.processing &&
                            <Text style={[s.bt, { color: !this.canCancel(item) ? 'gray' : 'orange' }]} >Cancel Appointment</Text>
                        }

                        {
                            item.processing &&
                            <View style={s.spinner}>
                                <ActivityIndicator color={'orange'} />
                            </View>
                        }

                    </TouchableOpacity>


                    <TouchableOpacity style={s.btInnerWrapper} onPress={() => {
                        this.state.navigation.navigate('AppointmentDetail', {
                            appointment: item
                        })
                    }}>
                        <Text style={s.bt}>Check Reports</Text>
                    </TouchableOpacity>
                </View>




            </View >
        );

        return (
            <SafeAreaView style={{ flex: 1 }}>


                <View style={s.container}>

                    <View style={s.titleContainer}>
                        <Text style={s.pageTitle}>Appointments</Text>
                        <TouchableOpacity onPress={() => { this.state.navigation.navigate('Book') }}>
                            <View style={s.filterBt}>
                                <Ionicons size={30} name='md-add' color='black' />
                            </View>
                        </TouchableOpacity>
                    </View>

                    {
                        this.state.appointments.length <= 0 ?
                            <View style={s.noData}>
                                <Image style={s.noDataImg} source={require('../assets/images/not_found.png')} />
                                <Text style={s.NoDataText}>You don't have any appointments....</Text>
                            </View>
                            :
                            <FlatList
                                data={this.state.appointments}
                                renderItem={renderItem}
                                key={item => item.id}
                                onRefresh={() => this.getAppointments()}
                                refreshing={this.state.isFetching}
                            />


                    }


                    <StatusBar style="auto" />
                </View>
            </SafeAreaView>
        );
    }
}

const s = StyleSheet.create({
    container: {
        flex: 1,
    },
    titleContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 14,
        paddingBottom: 10,
        paddingLeft: 14,
        paddingRight: 14
    },
    pageTitle: {
        fontSize: 30,
        fontWeight: '800'
    },
    item: {
        padding: 20,
        paddingBottom: 10,
        backgroundColor: 'white',
        borderRadius: 4,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    top: {
        display: 'flex',
        flexDirection: 'row'
    },
    filterBt: {
        width: 40,
        height: 40,
        borderRadius: 20,
        display: 'flex',
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    itemTitle: {
        fontSize: 14,
        color: 'black',
        fontWeight: '600'
    },

    itemDateWrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 'auto'
    },
    itemDate: {
        fontSize: 12,
        marginLeft: 4
    },
    bottom: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tests: {
        width: '100%',
        marginTop: 20

    },
    testsTitle: {
        width: '100%',
        textAlign: 'left',
        color: 'gray',
        fontSize: 10,
        textTransform: 'uppercase',
        fontWeight: '600',
    },
    bottomItem: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomItemTitle: {
        color: 'gray',
        fontSize: 10,
        textTransform: 'uppercase',
        fontWeight: '600',
        textAlign: 'center'
    },
    bottomItemValue: {
        color: 'black',
        fontSize: 12,
        marginTop: 8,
        textTransform: 'capitalize'
    },
    hSpacer: {
        width: 1,
        height: 20,
        marginLeft: 4,
        marginRight: 4,
        backgroundColor: 'lightgray'
    },
    itemActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginLeft: 'auto',
        marginTop: 20,
    },
    itemAction: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
        paddingTop: 3,
        paddingBottom: 3,
        borderWidth: 0.5,
        borderColor: '#EF5350',
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 20
    },
    itemActionText: {
        fontSize: 12,
        color: '#EF5350',
        marginLeft: 4,
        fontWeight: '600',
        marginRight: 4
    },
    line: {
        width: '100%',
        height: 0.25,
        backgroundColor: 'orange',
        marginTop: 30,
    },
    btWrapper: {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 10
    },
    btInnerWrapper: {
        flex: 1,

    },
    spinner: {
        width: '100%',
        paddingTop: 10,
    },
    bt: {
        width: '100%',
        color: 'orange',
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 10,
        borderTopColor: 'orange',
        borderTopWidth: 1,
    },
    btDisable: {
        color: 'lightgray'
    },
    noData: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fefefe'
    },
    noDataImg: {
        height: 140,
        resizeMode: 'contain',
    },
    NoDataText: {
        color: 'gray',
        fontSize: 14,
        marginTop: 20
    }
});
