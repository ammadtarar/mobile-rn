import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, FlatList } from 'react-native';
const Packages = require('../assets/data/packages');
import { SearchBar } from 'react-native-elements';

export default function App({ navigation }) {


    const [packages, setPackages] = useState(Packages);
    const [search, setSearch] = useState('')

    const renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => {
            navigation.navigate('PackageDetail', {
                packData: item
            })
        }}>
            <View style={s.item}>
                <Image style={s.ItemBg} source={item.img} />
                <View style={s.titleCont}>
                    <Text style={s.itemTitle}>{item.name}</Text>
                    <Text style={s.price}>Rs.{item.price}</Text>
                </View>
            </View >
        </TouchableOpacity >
    );

    function updateSearch(search) {
        setSearch(search);
        applyFilters();
    };

    function onClear() {
        clearFilters()
    }

    function applyFilters() {
        if (!search || search.length <= 1) {
            setPackages(Packages);
            return
        }
        var filtered = [];
        Packages.forEach(item => {
            if (item.name.toLowerCase().includes(search.toLowerCase())) {
                filtered.push(item)
            }
        });
        setPackages(filtered);

    }

    function clearFilters() {
        setSearch('');
        setPackages(Packages);

    }


    return (
        <View style={s.container}>

            <SearchBar
                placeholder="Enter a keyword to search..."
                onChangeText={updateSearch}
                value={search}
                onClear={onClear}
                lightTheme={true}
                containerStyle={{
                    padding: 10,
                    backgroundColor: 'white'
                }}
                inputContainerStyle={{
                    backgroundColor: '#EEEEEE',
                    height: 40,
                    borderRadius: 40
                }}
                inputStyle={{
                    fontSize: 14,
                }}
            />


            <FlatList
                data={packages}
                renderItem={renderItem}
                keyExtractor={item => item.name}
            />

            <StatusBar style="auto" />
        </View>
    );
}

const s = StyleSheet.create({
    container: {
        flex: 1,
    },
    filterBtWrapper: {
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 2
    },
    filterBt: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowRadius: 10,
        shadowOpacity: 0.4
    },
    item: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        height: 150,
        backgroundColor: 'white',
        borderRadius: 4,
        zIndex: 2,
        overflow: 'hidden',
        position: 'relative',
    },
    ItemBg: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 1
    },
    titleCont: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        position: 'relative',
        top: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        width: '100%',
        height: '100%',
        zIndex: 2,
        padding: 20

    },
    price: {
        fontSize: 14,
        fontWeight: '600',
        marginLeft: 'auto',
        marginTop: 30,
        color: 'white',
        backgroundColor: 'orange',
        padding: 4,
    },
    itemTitle: {
        fontSize: 14,
        fontWeight: '800',
        maxWidth: '70%',
        color: 'white',
    },
    sampleName: {
        fontSize: 12,
        color: 'gray',
        marginTop: 10
    },
    filtersWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 2,
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: '100%',
        height: '100%',
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    filters: {
        backgroundColor: 'white',
        width: '90%',
        padding: 20,
        paddingBottom: 10,
        shadowColor: 'gray',
        shadowRadius: 10,
        shadowOpacity: 0.2,
        borderRadius: 10
    },
    filtersTop: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    filtersTitle: {
        color: 'black',
        fontSize: 18,
        fontWeight: '700',
        marginLeft: 10
    },
    filtersBottom: {
        marginTop: 20,
        borderTopColor: 'lightgray',
        borderTopWidth: 0.5,
        paddingTop: 20
    },
    spacer: {
        height: 20
    },
    filtersActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20
    },
    btWrapper: {
        flex: 1,
        height: 50,
    },
    hSpacer: {
        width: 10,
        height: 50
    },
});
