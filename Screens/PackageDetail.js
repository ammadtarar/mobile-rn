import React, { useState, useEffect } from 'react';
import { ScrollView, SafeAreaView, Text, View, StyleSheet, Image, FlatList } from 'react-native';
export default function App({ route, navigation }) {

    const [packageData, setPackageData] = useState('');
    const { packData } = route.params;
    useEffect(() => {
        setPackageData(packData)
        console.log('TESTS');

    }, []);



    const renderItem = ({ item }) => (

        <View style={s.item}>
            <Text style={s.itemText}>- {item}</Text>
        </View >
    );

    return (
        <ScrollView style={s.container}>
            <Image style={s.ItemBg} source={packageData.img} />
            <View style={s.wrapper}>
                <Text style={s.title}>{packData.name}</Text>
                <Text style={s.price}>Rs. {packData.price}</Text>
                <View style={s.testsWrapper}>
                    <Text style={s.header}>Tests Included : </Text>
                    <FlatList
                        data={packageData.items}
                        renderItem={renderItem}
                        key={item => item.id}
                    />

                </View>
            </View>


        </ScrollView>

    );
}


const s = StyleSheet.create({
    container: {
        flex: 1
    },
    ItemBg: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
        backgroundColor: '#FFF3E0',
    },
    wrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        padding: 20

    },
    title: {
        fontSize: 22,
        fontWeight: '400',
        marginBottom: 6,
        color: 'gray'
    },
    price: {
        width: '40%',
        color: 'orange',
        fontSize: 16,
        fontWeight: '700',
    },
    testsWrapper: {
        marginTop: 20,
        width: '100%',
        paddingBottom: 5,
        backgroundColor: '#FAFAFA',
        borderRadius: 10,
        overflow: 'hidden'
    },
    header: {
        fontWeight: '600',
        padding: 10,
        backgroundColor: '#FFCC80',
        textTransform: 'uppercase',
        fontSize: 14,
        color: 'black',
        marginBottom: 5
    },
    item: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10

    },
    itemText: {
        fontSize: 13,
        fontWeight: '400'
    }
})